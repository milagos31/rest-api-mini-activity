const router = require("express").Router();
const productController = require("../controllers/productController");

//GET ALL
router.get("/", (req, res) => {
    productController.getProducts().then((results) => res.send(results));
});

//GET BY ID
router.get("/:id", (req, res) => {
    productController.getProductById(req.params.id).then((result) => {
        res.send(result);
    });
});

//POST PRODUCT
router.post("/create", (req, res) => {
    productController.postProduct(req.body).then((results) => res.send(results));
});

//DELETE PRODUCT
router.delete("/delete/:id", (req, res) => {
    productController.deleteProduct(req.params.id).then((removed) => res.send({ deleted: removed }));
});

module.exports = router;
