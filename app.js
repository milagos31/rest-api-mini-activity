const express = require("express");
const app = express();
const port = 4000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//Import route here
const productRoute = require("./routes/productRoute");
app.use("/products", productRoute);

const mongoose = require("mongoose");
const uri = "mongodb+srv://admin:admin1234@zuitt-bootcamp.inrmr.mongodb.net/s32?retryWrites=true&w=majority";
mongoose.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true });

app.listen(port, () => console.log("Server is running at port: " + port));
