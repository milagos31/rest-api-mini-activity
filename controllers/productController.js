const Product = require("../models/product");

//GET ALL PRODUCTS
module.exports.getProducts = () => {
    return Product.find({}).then((result) => {
        return result;
    });
};

//GET PRODUCT BY ID
module.exports.getProductById = (id) => {
    return Product.findById(id).then((result, err) => {
        if (err) {
            console.log(err);
            return false;
        } else return result;
    });
};

//POST PRODUCT
//CHECK FOR DUPLICATES
module.exports.postProduct = (requestBody) => {
    return Product.findOne({ name: requestBody.name }).then((found, err) => {
        if (found == null && requestBody.name != null && requestBody.price != null) {
            console.log("New product");
            const newProduct = new Product({
                name: requestBody.name,
                price: requestBody.price,
            });

            return newProduct.save().then((result, err) => {
                if (err) {
                    console.log(err);
                    return false;
                } else return result;
            });
        } else if (requestBody.name == null || requestBody.price == null) {
            console.log("incomplete details");
            return "incomplete details";
        } else {
            console.log("already exist");
            return "already exist";
        }
    });
};

//DELETE PRODUCT BY ID
module.exports.deleteProduct = (id) => {
    return Product.findByIdAndRemove(id).then((removed, err) => {
        if (err) {
            console.log(err);
            return false;
        } else return removed;
    });
};
